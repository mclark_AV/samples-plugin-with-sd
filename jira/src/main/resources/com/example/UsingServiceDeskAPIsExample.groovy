package com.example

//https://scriptrunner.adaptavist.com/latest/jira/blog/GettingMoreOutOfJsd.html#_adding_organizations_when_a_service_desk_issue_gets_created

import com.atlassian.fugue.Option
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.servicedesk.api.ServiceDesk
import com.atlassian.servicedesk.api.ServiceDeskManager
import com.atlassian.servicedesk.api.organization.OrganizationService
import com.atlassian.servicedesk.api.organization.OrganizationsQuery
import com.atlassian.servicedesk.api.util.paging.LimitedPagedRequest
import com.atlassian.servicedesk.api.util.paging.LimitedPagedRequestImpl
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import com.onresolve.scriptrunner.runner.customisers.WithPlugin

@WithPlugin("com.atlassian.servicedesk")

@PluginModule
ServiceDeskManager serviceDeskManager

@PluginModule
OrganizationService organizationService

MutableIssue issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("TSD-1")

def serviceDeskProject = serviceDeskManager.getServiceDeskForProject(issue.projectObject) as ServiceDesk

// if the project is not a Service Desk one then do nothing
if (serviceDeskProject.isLeft()) {
    log.error "${serviceDeskProject?.left()?.get()}"
    return
}

def serviceDeskId = serviceDeskProject?.right()?.get()?.id as Integer
